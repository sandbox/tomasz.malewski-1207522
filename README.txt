Freespace theme for Drupal
Created by Tomasz Malewski

The purpose is to create as many screen FREESPACE as possible.
Modern layouts consume too much free space requires over 1280x1024 resolution, constant scrolling big top banner begins really annoying.
I want to make a minimal backend theme to let portal crew smoother manage content with all space consuming FX.

Any comments: varsovie@o2.pl

____________________________________

// $Id: README.txt,v 1.4.2.3 2011/02/18 05:26:30 andregriffin Exp $

Framework 3.x for Drupal 7
Created by André Griffin
http://drupal.org/project/framework

Framework is a blank canvas for theme developers. 
Use Framework as a user friendly starting point to help facilitate your theme development. 
Build site themes without feeling like you have to reinvent the wheel or remove unnecessary code every time.

Framework 3 is intended to be used with the "Seven" administration theme included with Drupal 7.

Framework is actively developed and supported on my own time. If you would like to say thanks, please consider donating via:
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=1532730

Development sites:
http://d7.andregriffin.com
http://d6.andregriffin.com

-------
USAGE

Install to sites/all/themes

To fully convert Framework to your own custom theme, it is encouraged that you: 
 - Copy the 'framework' directory within sites/all/themes/ and rename it as 'yourthemename' 
 - Change the filename and contents of the .info file accordingly
 - Use find/replace in template.php to replace 'framework' with 'yourthemename' 
 - Upload and enable the theme
 - Modify as needed

To create a sub-theme based on Framework, see: http://drupal.org/node/225125

-------
SUPPORT

If you have questions or problems, check the issue list before submitting a new issue: 
http://drupal.org/project/issues/framework

For general support, please refer to:
http://drupal.org/support