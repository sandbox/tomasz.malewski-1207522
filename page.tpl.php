<?php
// $Id: page.tpl.php,v 1.4.2.6 2011/02/18 05:26:30 andregriffin Exp $
?>
<div id="wrapper" class="clearfix">

  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    <?php if ($main_menu): ?>
      <a href="#navigation" class="element-invisible element-focusable"><?php print t('Skip to navigation'); ?></a>
    <?php endif; ?>
  </div>

  <header id="header" role="banner" class="clearfix">
	<table id="header-table">
	<tr>
		<td>
		<?php if ($page['sidebar_second']): ?>
			<aside id="sidebar_second" role="complimentary" class="sidebar clearfix">
			<?php print render($page['sidebar_second']); ?>
			</aside>  <!-- /#sidebar-second -->
		<?php endif; ?>
		</td>
		<td>	
			<?php if ($site_name || $site_slogan): ?>
			<?php if ($logo): ?>
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
			<img width=15 src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
			</a>
			<?php endif; ?>
        <?php if ($site_name): ?>
          <?php if ($title): ?>
		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
	<?php else: /* Use h1 when the content title is empty */ ?>
            	<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          <?php endif; ?>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <?php print $site_slogan; ?>
        <?php endif; ?>
    <?php endif; ?> >
		<?php print $title; ?>
		</td>
		<td>
		<nav id="navigation" role="navigation" class="clearfix">
			<?php if ($secondary_menu): print $secondary_menu; endif; ?>
		</nav>
		</td>
		<td>
		</td>
	</tr>
	</table>
  </header> <!-- /#header -->
	<?php print render($page['header']); ?>


  <?php if ($main_menu || $secondary_menu): ?>
    <nav id="navigation" role="navigation" class="clearfix">
      <?php if ($page['navigation']): ?> <!--if block in navigation region, override $main_menu and $secondary_menu-->
        <?php print render($page['navigation']); ?>
      <?php endif; ?>
      <?php if (!$page['navigation']): ?>
        <?php if ($main_menu): print $main_menu; endif; ?>

      <?php endif; ?>
    </nav> <!-- /#navigation -->
  <?php endif; ?>

<br>

  <section id="main" role="main" class="clearfix">
    <?php if ($breadcrumb): print $breadcrumb; endif;?>
    <?php print $messages; ?>
    <a id="main-content"></a>
    <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
    <?php print render($title_prefix); ?>
    <?php 
	/* if ($title): ?><div class="title" id="page-title"><?php print $title; ?></div><?php endif;  	*/ ?>
    <?php print render($title_suffix); ?>
    <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php endif; ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
    <?php print render($page['content']); ?>
  </section> <!-- /#main -->
  
  <?php if ($page['sidebar_first']): ?>
    <aside id="sidebar-first" role="complimentary" class="sidebar clearfix">
      <?php print render($page['sidebar_first']); ?>
    </aside>  <!-- /#sidebar-first -->
  <?php endif; ?>

  <?php if ($page['sidebar_second']): ?>
    <aside id="sidebar-second" role="complimentary" class="sidebar clearfix">
      <?php print render($page['sidebar_second']); ?>
    </aside>  <!-- /#sidebar-second -->
  <?php endif; ?>

  <footer id="footer" role="contentinfo" class="clearfix">
    <?php print render($page['footer']) ?>
    <?php print $feed_icons ?>
  </footer> <!-- /#footer -->

</div> <!-- /#wrapper -->
